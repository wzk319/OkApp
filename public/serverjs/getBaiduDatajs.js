/**
 * Created by wangzhikui on 2016/7/30.
 * 该方法是获取百度APIstore提供的数据，获取数据时只需要传递一个api接口的路径，
 * 和回调函数，回调函数接收一个参数，该参数是获取的数据；
 */
var http = require("http");
var baiduDataApiObj ={};

baiduDataApiObj.rootServise = function(path,callback) {

    var opt={
        hostname:"apis.baidu.com",
        port:80,
        path:path,
        headers:{"apikey":"308231d18135c3c885706376d8f385f9"},
        agent:false
    }
    http.get(opt,function (res) {
        var html="";
        res.on("data",function (data) {
            html+=data;
        })
        res.on("end",function () {
            (typeof callback=="function")&&callback(html);
        })
    })


}
baiduDataApiObj.getCityInfo = function (cityName,callback) {
    var cityName = encodeURI(cityName);
    baiduDataApiObj.rootServise("/apistore/weatherservice/cityinfo?cityname="+cityName,callback);
}
baiduDataApiObj.getWeather = function (cityName,callback) {
    baiduDataApiObj.getCityInfo(cityName,function (data) {
       
        var data = JSON.parse(data);
        var cityid=data.retData.cityCode;
        baiduDataApiObj.rootServise("/apistore/weatherservice/recentweathers?cityname="+cityName+"&cityid="+cityid,callback)
    })

}

var trainBaseURl="/qunar/qunar_train_service";

baiduDataApiObj.getTrainInfo = function (trainNum,from,to,date,callback) {
    baiduDataApiObj.rootServise(trainBaseURl+"/traindetail?version=1.0"+"&train="+trainNum+"&from="+from+"&to="+to+"&date="+date,callback);
    
}
baiduDataApiObj.getTrainSuggest = function (key,callback) {
    var key = encodeURI(key);
    baiduDataApiObj.rootServise(trainBaseURl+"/suggestsearch?version=1.0"+"&keyword="+key,callback)
}
baiduDataApiObj.getStationToStationInfo = function (from,to,date,callback) {
    var from = encodeURI(from);
    var to = encodeURI(to);
    baiduDataApiObj.rootServise(trainBaseURl+"/s2ssearch?version=1.0"+"&from="+from+"&to="+to+"&date="+date,callback)
}


module.exports=baiduDataApiObj;

