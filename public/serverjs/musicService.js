/**
 * Created by wangzhikui on 2016/8/2.
 */
var http = require("http");
var music = {};
music.rootServise=function (url,callback) {
    http.get(url,function (res) {
        var html="";
        res.on('data',function (body) {
            html+=body;
        }).on('end',function () {
            (typeof callback=="function")&&callback(html);
        })
    })

}
music.getMusicList = function (callback) {
    music.rootServise("http://i.y.qq.com/v8/fcg-bin/v8_4web.fcg?&format=json",callback)
}
music.musicSuggest = function (key,callback) {
    music.rootServise("http://i.y.qq.com/s.plcloud/fcgi-bin/smartbox_new.fcg?key="+key+"&format=jsonp&jsonpCallback=MusicJsonCallBack",callback)

}
music.searchlyric = function (songname,duration,hash,callback) {
    music.rootServise("http://lyrics.kugou.com/search?ver=1&man=no&client=pc&keyword="+songname+"&duration="+duration+"&hash="+hash,callback)
}
music.searchlyrics = function (songname,callback) {
    music.rootServise("http://lyricsearch.kugou.com/lyric_search?keyword="+songname+"&page=1&pagesize=40",callback)
}
music.getlyric = function (id,accesskey,callback) {
    music.rootServise("http://lyrics.kugou.com/download?ver=1&id="+id+"&accesskey="+accesskey+"&fmt=lrc",callback)
}
music.getlyricBysnameAndTime = function (sname,duration,callback){
    music.searchlyrics(sname,function (data) {
        var hash ="";
        var sname=""
        var data = JSON.parse(data).data.lists;
        for(var key in data){
           if(data[key].TimeLength==duration){
            sname = data[key].SongName;
            hash = data[key].FileHash;
               break;
           }
        }
        console.log("-------")
        console.log(sname)
        music.searchlyric(encodeURI(sname),0,hash,function (data) {
            var data = JSON.parse(data).candidates[0];
            music.getlyric(data.id,data.accesskey,callback);
            
            console.log(data)
        })

    });
}
module.exports = music;