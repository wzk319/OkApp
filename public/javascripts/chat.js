/**
 * Created by wangzhikui on 2016/8/14.
 */

var socket = io.connect("http://127.0.0.1:3000");
var send = document.querySelector(".send");
var input = document.querySelector(".input");
var userImageDiv=$(".userImg");
var modal = $(".modal")
var imageList = $(".imgList");
var userInfo={};
var user="";
var userImage=$(".userImg img")
var imgListArray=[];
var defaultImag="../head/default.png";
var owerImage={};
var otherImage={}
function $(selector) {
    return document.querySelector(selector);
}
//浏览器通知
function notify(title,mes) {
    if(window.Notification&&Notification.permission!="denied") {
        Notification.requestPermission(function (status) {
            var notify = new Notification(title,{body:mes});
            notify.onshow = function () {
                setTimeout(notify.close.bind(notify),2000);
            }
        })
    }
}
$(".autograph").onfocus =function () {
    // var textRange = document.createTextRange();
    // textRange.
}
//声明和实现获取json数据的函数
function getJson(url,callback) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState==4)
        {
            if (xhr.status==200)
            {
                callback(xhr.responseText)
            }
            else
            {

            }
        }

    }
    xhr.open("get",url,true);
    xhr.send(null);

}
//获取头像列表
var imgList = $(".imgList");
getJson("/getHeadList",function (data) {
    JSON.parse(data).forEach(function (item) {
        var img = new Image();
        img.src="../head/"+item;
        imgList.appendChild(img);
    })
});
//弹出模态框
userImageDiv.onclick  =function () {
    modal.style.display="block";
}
//为头像列表绑定事件，当点击头像时更改头像并通知服务器更新
imageList.onclick = function (e) {
    if(e.target.hasAttribute("src")){
         userImage.src=e.target.src
        socket.emit("imgChange",user,userImage.src);
    }
    modal.style.display="none";
}

send.onclick = function () {
    if(input.value){
        socket.emit("sendMSG",user,input.value);
        input.value = '';
    }
}

socket.on("state",function (sev,mes) {
    notify("",mes);
})
socket.on("userleave",function (mes) {
    notify("",mes);
})
socket.on("connect",function () {
    user = prompt("输入姓名")||"游客";
    $(".userNick").innerText = user;
    socket.emit("join",user,$(".userImg>img".src));
})
socket.on("chat",function (userd,data) {
    var p = document.createElement("div");
    var img = document.createElement("div");
    var username = document.createElement("span");
    var content = document.createElement("span");

    img.className="touxiang";
    username.className="username";
    content.className="content";
    username.innerText=userd;
    content.innerText=data;
    if(userd==user){
        img.style.backgroundImage="url("+$(".userImg img").src+")";
        p.className="msg left";
        p.appendChild(img);
        p.appendChild(username);
        p.appendChild(content);

    }else{
        var bg=(imgListArray[userd])?imgListArray[userd]:defaultImag;
        img.style.backgroundImage="url("+bg+")";
        p.className="msg right";
        p.appendChild(content);
        p.appendChild(username);
        p.appendChild(img);
    }

    $(".rooms").appendChild(p);
    $(".rooms").scrollTop = $(".rooms").scrollHeight;


})
socket.on("userImageList",function (userImageList) {
   imgListArray = userImageList
})


