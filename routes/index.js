var express = require('express');
var baiduapiObj = require("../public/serverjs/getBaiduDatajs");
var musicObj = require("../public/serverjs/musicService");
var getFile = require("../public/serverjs/getFile");
var formidable = require("formidable");
var fs = require("fs");
var router = express.Router();
var GB2312UnicodeConverter = {
  ToUnicode: function (str) {
    return escape(str).toLocaleLowerCase().replace(/%u/gi, '\\u');
  }
  , ToGB2312: function (str) {
    return unescape(str.replace(/\\u/gi, '%u'));
  }
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/getWeather',function (req,res,next) {
  var cityName = req.query.cityname
  baiduapiObj.getWeather(cityName,function (data) {
    console.log(data)
    var data = GB2312UnicodeConverter.ToGB2312(data)
    res.json(data)
  })
  
})
router.get('/getStationToStationInfo',function (req,res,next) {
  var from = req.query.from,
      to =req.query.to,
      date = req.query.date;
  baiduapiObj.getStationToStationInfo(from,to,date,function (data) {
    res.json(data)
  })
  
})
router.get('/getTrainSuggest',function (req,res,next) {
  var key =req.query.key;
  baiduapiObj.getTrainSuggest(key,function (data) {
    res.json(data)
  })
})
router.get('/gettrainInfo',function (req,res,next) {
  var trainNum=req.query.trainnum;
  var from=encodeURI(req.query.from);
  var to  = encodeURI(req.query.to);
  var date  = req.query.date;
  baiduapiObj.getTrainInfo(trainNum,from,to,date,function (data) {
    res.json(data)
  })
})
router.get('/getlyric',function (req,res,next) {
  var sname = req.query.sname;
  var duration = req.query.duration;
  musicObj.getlyricBysnameAndTime(encodeURI(sname),duration,function (data) {
    // console.log(data)
    var data = JSON.parse(data).content;
    data = new Buffer(data,"base64")
    res.json(data.toString())
  })
})
router.get('/getMusic',function (req,res,next) {
  musicObj.getMusicList(function (data) {
    var data = data.replace(/[_\-$\w\d]{0,}\(/,"").replace(/\)$/,"");
    res.json(data);
  })
  
})
router.get('/searchMusic',function (req,res,next) {
  
})
router.get('/getHeadList',function (req,res,next) {
  var head = getFile.getImageFiles('./public/head/');
  // console.log(head)
  res.json(head);
})
function rename(newPath,oldpath) {

}
router.post('/upload',function (req,res,next) {
  var form = new formidable.IncomingForm();
    form.uploadDir="./upload";
    form.keepExtensions=true;
    var myFiles=[];
    form.parse(req,function (err,fields,files) {
    })
    form.on("fileBegin",function (name,files) {
        var myFile={};
        myFile.path=files.path;
        myFile.filename=files.name;
        myFiles.push(myFile);
        })
    form.on('end',function () {
            /*myFiles.forEach(function (item) {
                let oldpath = form.uploadDir+"/"+item.filename;
                if(!fs.existsSync(oldpath)){
                    fs.rename(item.path,oldpath,function () {
                    })
                }else{
                }

            });*/
        // return;
        res.json({
            success:"ok"
        })
    })
})
// console.log(myFiles)
router.post("/postmes",function (req,res,next) {
    var urls = req.body;
    console.log(urls)
    // var isTaobao=url.test(/taobao.com/);
    // console.log(isTaobao);
    if(!!urls.url){
        res.json({
            authorized:true,
            isShop:true,
            logged:true
        })
    }
    else {
        res.json({
        })
    }

    /*var name=req.query.name;
    console.log(req)
    if(!req.query.name){
        res.json({
            status:"error",
            mes:"缺少参数"
        })

    }else{
        res.json({
            authorized:true,
            isShop:true,
            logged:true
        })
    }*/

})
module.exports = router;
