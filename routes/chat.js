/**
 * Created by wangzhikui on 2016/8/9.
 */
var express = require('express');
var router = express.Router();
var socket_io = require('socket.io');
Array.prototype.removeItems = function (item) {
 let index = this.indexOf(item);
    if(index>-1){
        this.splice(index,1);
        this.removeItems(item);
    }
}
Object.prototype.deleteProp=function (o) {
    delete this[o]
}
router.prepareSocketIo = function (server) {
    var users = [];
    var userList={};
    var userImageList ={};
    
    var io = socket_io.listen(server);
    io.on("connection",function (socket) {
        socket.on("join",function (user,userImage) {
            socket.user = user;
            userList[user]=socket.id;
           users.push(user);
            socket.broadcast.emit("state","SERVER",user+"上线了");
            userImageList[user]=userImage;
            socket.broadcast.emit("userImageList",userImageList)
            
        });
        socket.on("disconnect",function () {
            console.log(socket.user+"下线了");
            socket.broadcast.emit("userleave",socket.user+"下线了")
            users.removeItems(socket.user);
            userList.deleteProp(socket.user)
            console.log(userList);
        })

        socket.on("sendMSG",function (user,msg) {
            console.log(socket.id+"--------test")
            socket.emit("chat",user,msg);//给客户端发消息
            socket.broadcast.emit("chat",user,msg)//给除了发送端的客户端发送消息
            socket.broadcast.to(userList["22"]).emit("chat",user,"22--"+msg);//给特定的人发送消息


        })
        socket.on("imgChange",function (user,userImage) {
            userImageList[user]=userImage;
            socket.broadcast.emit("userImageList",userImageList)
        })



        
    })

}
module.exports = router;
